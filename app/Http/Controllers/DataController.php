<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Data;
use Illuminate\Support\Facades\Validator;


class DataController extends Controller
{
    public function index()
    {
        $data = Data::all();
        return response()->json([
            'data' => $data
        ]);
    }

    public function create(Request $request)
    {
        if ($request->age > 17) {

            return "You must be 18 years old to sign up";
        } else {
            $rules = array(
                'nama' => 'required',
                'alamat' => 'required',
                'umur' => 'required',
                'pekerjaan' => 'required'
            );
            $messages = [
                'nama.required' => 'Please enter a name.',
                'alamat.required' => 'Please enter a address.',
                'umur.required' => 'Please enter a age.',
                'pekerjaan.required' => 'Please enter your job.'
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                return response()->json([
                    "error" => 'validation error',
                    'message' => $validator->errors($errors, 500)
                ]);
            } else {
                Data::create([
                    'nama' => $request->nama,
                    'alamat' => $request->alamat,
                    'umur' => $request->umur,
                    'pekerjaan' => $request->pekerjaan,
                ]);

                return response()->json([
                    'success' => 'Success',
                    'message' => 'Data has been added'
                ]);
            }
        }
    }


    public function findUpdate($id)
    {
        $data = Data::find($id);
        return response()->json([
            'success' => True,
            'data' => $data
        ]);
    }


    public function update(request $request, $id)
    {
        $nama = $request->nama;
        $alamat = $request->alamat;
        $umur = $request->umur;
        $pekerjaan = $request->pekerjaan;

        $data = Data::find($id);

        $data->nama = $nama;
        $data->alamat = $alamat;
        $data->umur = $umur;
        $data->pekerjaan = $pekerjaan;

        $data->save();

        return response()->json([
            'success' => TRUE,
            'messasge' => "Your Data Has Been Updated"
        ]);
    }

    public function delete($id)
    {
        $data = Data::find($id);
        $data->delete();

        return response()->json([
            'success' => TRUE,
            'messages' => 'Your Data Has Been Deleted'
        ]);
    }
}

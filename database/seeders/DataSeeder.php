<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

Use App\Data;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 10; $i++){
            
                    DB::table('data')->insert([
                        'nama' => $faker->name,
                        'alamat' => $faker->address,
                        'umur' => $faker->numberBetween(20,30),
                        'pekerjaan' => $faker->jobTitle,
                        
                    ]);

        };
    }
}
